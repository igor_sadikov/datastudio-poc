<?php

/**
 * @author    Igor Sadikov <isadikov@absolunet.com>
 * @copyright Copyright (c) Absolunet (https://www.absolunet.com)
 * @link      https://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Analytics\Api\Data;

interface DateRangeInterface
{
    /**
     * @return string|null
     */
    public function getFrom(): ?string;

    /**
     * @param string $from
     *
     * @return void
     */
    public function setFrom(string $from): void;

    /**
     * @return string|null
     */
    public function getTo(): ?string;

    /**
     * @param string $to
     *
     * @return void
     */
    public function setTo(string $to): void;
}
