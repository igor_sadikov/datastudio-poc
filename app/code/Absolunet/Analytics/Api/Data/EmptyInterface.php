<?php

/**
 * @author    Igor Sadikov <isadikov@absolunet.com>
 * @copyright Copyright (c) Absolunet (https://www.absolunet.com)
 * @link      https://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Analytics\Api\Data;

interface EmptyInterface
{

}
