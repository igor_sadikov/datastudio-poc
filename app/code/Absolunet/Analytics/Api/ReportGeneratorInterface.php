<?php

/**
 * @author    Igor Sadikov <isadikov@absolunet.com>
 * @copyright Copyright (c) Absolunet (https://www.absolunet.com)
 * @link      https://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Analytics\Api;

use Absolunet\Analytics\Api\Data\DateRangeInterface;
use Absolunet\Analytics\DataModel\Record;

interface ReportGeneratorInterface
{
    /**
     * @param string $type
     * @param \Absolunet\Analytics\Api\Data\DateRangeInterface|null $dateRange
     *
     * @return \Absolunet\Analytics\Api\Data\EmptyInterface
     */
    public function get(string $type, ?DateRangeInterface $dateRange = null): Record;
}
