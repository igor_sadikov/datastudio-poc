<?php

/**
 * @author    Igor Sadikov <isadikov@absolunet.com>
 * @copyright Copyright (c) Absolunet (https://www.absolunet.com)
 * @link      https://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Analytics\DataModel;

use Absolunet\Analytics\Api\Data\DateRangeInterface;

class DateRange implements DateRangeInterface
{
    /** @var string|null */
    private $from = null;

    /** @var string|null */
    private $to = null;

    public function getFrom(): ?string
    {
        return $this->from;
    }

    public function setFrom(string $from): void
    {
        $this->from = $from;
    }

    public function getTo(): ?string
    {
        return $this->to;
    }

    public function setTo(string $to): void
    {
        $this->to = $to;
    }
}
