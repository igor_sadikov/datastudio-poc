<?php

/**
 * @author    Igor Sadikov <isadikov@absolunet.com>
 * @copyright Copyright (c) Absolunet (https://www.absolunet.com)
 * @link      https://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Analytics\DataModel;

use Absolunet\Analytics\Api\Data\EmptyInterface;
use Magento\Framework\DataObject;

class Record extends DataObject implements EmptyInterface
{

}
