<?php

/**
 * @author    Igor Sadikov <isadikov@absolunet.com>
 * @copyright Copyright (c) Absolunet (https://www.absolunet.com)
 * @link      https://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Analytics\Report;

use Magento\Analytics\ReportXml\Config;
use Magento\Framework\Exception\NoSuchEntityException;

class FilteredQueryConfig extends Config
{
    private $filters = [];

    public function addFilter(array $filter): void
    {
        $this->filters[] = $filter;
    }

    /**
     * @param string $queryName
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function get($queryName): array
    {
        $data = parent::get($queryName);

        if ($data === null) {
            throw new NoSuchEntityException(__('No such report type.'));
        }

        foreach ($this->filters as $filter) {
            $data['source']['filter'][] = $filter;
        }

        return $data;
    }
}
