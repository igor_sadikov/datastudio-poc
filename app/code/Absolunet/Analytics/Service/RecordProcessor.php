<?php

/**
 * @author    Igor Sadikov <isadikov@absolunet.com>
 * @copyright Copyright (c) Absolunet (https://www.absolunet.com)
 * @link      https://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Analytics\Service;

use Absolunet\Analytics\DataModel\Record;

class RecordProcessor
{
    public function execute(Record $record, array $result): array
    {
        return $record->getData();
    }
}
