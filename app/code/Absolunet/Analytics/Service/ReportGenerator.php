<?php

/**
 * @author    Igor Sadikov <isadikov@absolunet.com>
 * @copyright Copyright (c) Absolunet (https://www.absolunet.com)
 * @link      https://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Analytics\Service;

use Absolunet\Analytics\Api\Data\DateRangeInterface;
use Absolunet\Analytics\Api\ReportGeneratorInterface;
use Absolunet\Analytics\DataModel\Record;
use Absolunet\Analytics\DataModel\RecordFactory;
use Absolunet\Analytics\Report\FilteredQueryConfig;
use Magento\Analytics\ReportXml\ReportProvider;

class ReportGenerator implements ReportGeneratorInterface
{
    /** @var FilteredQueryConfig */
    private $queryConfig;

    /** @var ReportProvider */
    private $reportProvider;

    /** @var RecordFactory */
    private $recordFactory;

    public function __construct(
        FilteredQueryConfig $queryConfig,
        ReportProvider $reportProvider,
        RecordFactory $reportFactory
    ) {
        $this->queryConfig = $queryConfig;
        $this->reportProvider = $reportProvider;
        $this->recordFactory = $reportFactory;
    }

    public function get(string $type, ?DateRangeInterface $dateRange = null): Record
    {
        if ($dateRange !== null) {
            $this->queryConfig->addFilter($this->prepareFilter($dateRange));
        }

        $iterator = $this->reportProvider->getReport($type);
        $data = iterator_to_array($iterator);

        return $this->recordFactory->create(['data' => $data]);
    }

    private function prepareFilter(DateRangeInterface $dateRange): array
    {
        $condition = [];
        $conditionMap = [
            'gteq' => $dateRange->getFrom(),
            'lt' => $dateRange->getTo(),
        ];

        foreach ($conditionMap as $operator => $value) {
            if ($value !== null) {
                $condition[] = [
                    'attribute' => 'created_at',
                    'operator' => $operator,
                    '_value' => $value,
                ];
            }
        }

        return [
            'glue' => 'and',
            'condition' => $condition,
        ];
    }
}
